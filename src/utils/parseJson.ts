import * as path from 'path';
import * as fs from 'fs';

const SOURCE_DATA_PATH = path.join(__dirname, '../../data')

const parseJson = (fileName: string) => {
    const filePath = path.join(SOURCE_DATA_PATH, fileName);
    return JSON.parse(fs.readFileSync(filePath, 'utf-8'));
}

export default parseJson