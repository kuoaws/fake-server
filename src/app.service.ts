import { Injectable } from '@nestjs/common';
import parseJson from './utils/parseJson'

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }

  getNews() {
    return parseJson('news.json');
  }
}
